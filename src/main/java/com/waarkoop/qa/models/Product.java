package com.waarkoop.qa.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Product {
    private @Getter @Setter String provider;
    private @Getter @Setter String title;
    private @Getter @Setter String url;
    private @Getter @Setter String brand;
    private @Getter @Setter Float price;
    private @Getter @Setter String unit;
    private @Getter @Setter Boolean isPromo;
    private @Getter @Setter String promoDetails;
    private @Getter @Setter String image;
}