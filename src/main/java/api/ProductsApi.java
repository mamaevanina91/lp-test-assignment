package api;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

import static java.lang.String.format;

public class ProductsApi {
    private static final String BASE_SEARCH_URL = "https://waarkoop-server.herokuapp.com/api/v1/search/test/%s";

    @Step("Get products by name")
    public void getProductsByName(String productName) {
        SerenityRest.given()
                .get(format(BASE_SEARCH_URL, productName));
    }
}
