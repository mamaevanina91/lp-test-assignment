Feature: Search for the product

  @TEST_1
  Scenario Outline: Get Products by <productName>
    When the user gets all products for "<productName>"

    Then the user should receive status code 'OK'
    And the user should see each product title contains "<productName>"

    Examples:
      | productName |
      | apple       |
    #| mango       |    # BUG - not all items has title containing "mango"
    #| water       |    # BUG - not all items has title containing "water"
    #| tofu        |    # BUG - not all items has title containing "tofu"

  @TEST_2
  Scenario Outline: Each product has valid urls
    When the user gets all products for "<productName>"

    Then the user should receive status code 'OK'
    And the user should see each product has valid url

    Examples:
      | productName |
      | apple       |
    # | mango       |    # BUG - not all urls are valid, 403
    # | water       |    # BUG - not all urls are valid, 403, 502
    # | tofu        |    # BUG - not all urls are valid, 403

  @TEST_3
  Scenario Outline: Each product has valid image url
    When the user gets all products for "<productName>"

    Then the user should receive status code 'OK'
    And the user should see each product has valid image url

    Examples:
      | productName |
      | apple       |
      | mango       |
      | water       |
      | tofu        |

  @TEST_4
  Scenario Outline: If promo is applied, promo details shouldn't be empty
    When the user gets all products for "<productName>"

    Then the user should receive status code 'OK'
    And the user should see if promo is applied, promo details are not empty

    # checking for "apple" only due to mentioned bug in the readme
    Examples:
      | productName |
      | apple       |
     # | mango       |
     # | water       |
     # | tofu        |

  @TEST_5
  Scenario: Not existing product returns no results
    When the user gets all products for "car"

    Then the user should receive status code 'NOT_FOUND'
    And the user should see response details:
      | detail.error          | True                 |
      | detail.message        | Not found            |
      | detail.requested_item | car                  |
      | detail.served_by      | https://waarkoop.com |