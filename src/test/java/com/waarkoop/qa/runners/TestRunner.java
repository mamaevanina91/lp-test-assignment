package com.waarkoop.qa.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/search/GetProducts.feature",
        glue = "com/waarkoop/qa/stepdefinitions"

)
public class TestRunner {

}
