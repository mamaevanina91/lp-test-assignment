package com.waarkoop.qa.stepdefinitions;

import io.cucumber.java.en.Then;
import org.springframework.http.HttpStatus;

import java.util.Map;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.equalTo;

public class CommonStepsDefinitions {
    @Then("the user should receive status code {string}")
    public void verifyStatusCode(String statusCode) {
        int code = HttpStatus.valueOf(statusCode).value();
        restAssuredThat(response -> response.statusCode(code));
    }

    @Then("the user should see response details:")
    public void verifyResponseDetails(Map<String, String> details) {
        for (Map.Entry<String, String> entry : details.entrySet()) {
            if (entry.getKey().equals("detail.error")) {
                restAssuredThat(response -> response.body(entry.getKey(), equalTo(Boolean.parseBoolean(entry.getValue()))));
            } else {
                restAssuredThat(response -> response.body(entry.getKey(), equalTo(entry.getValue())));
            }
        }
    }
}