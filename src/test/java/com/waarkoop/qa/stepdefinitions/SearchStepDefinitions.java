package com.waarkoop.qa.stepdefinitions;

import api.ProductsApi;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;
import com.waarkoop.qa.models.Product;
import org.assertj.core.api.SoftAssertions;

import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

public class SearchStepDefinitions {

    @Steps
    public ProductsApi productsApi;

    @When("the user gets all products for {string}")
    public void theUserGetsProductsByName(String productName) {
        productsApi.getProductsByName(productName);
    }

    @Then("the user should see each product title contains {string}")
    public void theUserShouldSeeProductTitleContains(String productName) {
        List<Product> products = Arrays.asList(SerenityRest.lastResponse().getBody().as(Product[].class));
        Assertions.assertThat(products.stream().allMatch(p -> p.getTitle().toLowerCase().contains(productName)))
                .as(format("Verify all product titles contain \"%s\"", productName))
                .isTrue();
    }

    @Then("the user should see each product has valid url")
    public void theUserShouldSeeEachProductHasValidUrl() {
        List<Product> products = Arrays.asList(SerenityRest.lastResponse().getBody().as(Product[].class));
        SoftAssertions.assertSoftly(softly -> {
            for (Product p : products) {
                softly.assertThat(SerenityRest.get(p.getUrl()).statusCode())
                        .as("Check url validity")
                        .isEqualTo(200);
            }
        });
    }

    @Then("the user should see each product has valid image url")
    public void theUserShouldSeeEachProductHasValidImagedUrl() {
        List<Product> products = Arrays.asList(SerenityRest.lastResponse().getBody().as(Product[].class));
        SoftAssertions.assertSoftly(softly -> {
            for (Product p : products) {
                softly.assertThat(SerenityRest.get(p.getImage()).statusCode())
                        .as("Check image url validity")
                        .isEqualTo(200);
            }
        });
    }

    @Then("the user should see if promo is applied, promo details are not empty")
    public void theUserShouldSeeIfPromoIsAppliedDetailsAreNotEmpty() {
        List<Product> products = Arrays.asList(SerenityRest.lastResponse().getBody().as(Product[].class));
        SoftAssertions.assertSoftly(softly -> {
            for (Product p : products) {
                if (p.getIsPromo()) {
                    softly.assertThat(p.getPromoDetails())
                            .as("Check promo details are not empty")
                            .isNotEmpty();
                } else {
                    softly.assertThat(p.getPromoDetails())
                            .as("Check promo details are empty")
                            .isEmpty();
                }
            }
        });
    }
}
