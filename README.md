# Test Assignment for the position at the Lease Plan project: Automation QA 

Serenity BDD is a library that makes it easier to write high quality automated acceptance tests, with powerful reporting and living documentation features. It has strong support for both web testing with Selenium, and API testing using RestAssured.

This is a **[BDD](https://cucumber.io/docs/bdd/)** test project for API automated testing of **https://waarkoop-server.herokuapp.com/**.

1. Clone project from https://gitlab.com/mamaevanina91/lp-test-assignment
2. Import dependencies.

## What was refactored and why?
- Scenario names were added.
- 'post_product.feature' file was renamed to 'GetProducts.feature' - the previous filename didn't match to Java code convention, also, it's presented a GET-method, not POST, so the filename was changed too.
- added test annotation '@TEST_1', '@TEST_2' etc - to divide scenarios.
- deleted endpoint url from scenarios steps - it's better to keep endpoint url in variables.
- 'productName' variables were added to the Scenario's steps - the verification of all products was done with data-driven approach - Scenario Outline, to minimise the number of steps.
- *src/test/java/starter/stepdefinitions/CarsAPI.java* file was deleted from stepdefinitions package - /car is a non-existing endpoint.
- *src/main/java/api/ProductsApi.java* file was created instead CarsAPI.java in 'api' package - there were created a 'BASE_SEARCH_URL' and a base API method 'getProductsByName'.
- 'CommonStepsDefinitions' class was added to a stepdefinitions package - more general methods were moved to the CommonStepDefinitions.
- class TestRunner was moved to a separate 'runners' package - was updated a pass to the features files, also, the 'glue' was added.
- the 'product' model was added to *src/main/java/com/waarkoop/qa/models/Product.java* - to convert the product response.
- 'he_Doesn_Not_See_The_Results' method - had typos in the name and wasn't written within java conventions - was replaced with methods from 'CommonStepsDefinitions.class'.
- 'he_Doesn_Not_See_The_Results' method - initial implementation had assertion of incompatible types: boolean vs string.
- 'heSeesTheResultsDisplayedForMango' method was replaced with a parametrised method ('theUserShouldSeeProductTitleContains'), the initial assertion was not intended for checking arrays, replaced with Java stream.
- also it was observed that "promoDetails" in most cases has String type, but in some - Boolean. It is possible for json scheme to have multiple data types for the field, but in this case it seems better to have only String. This field has relation with "isPromo", and logically it's more clear, if promo exists. then "promoDetails" has some text, if not - an empty string;

## How to run tests?
Options to use:
1. From IDE, for example from IntelliJ IDEA:
  - to run a single test: right-click on the scenario in the *.feature* file->Run, (which creates Cucumber Java Configuration automatically);
  - all tests: right-click on the *runners/TestRunner*->Run
2. With Maven command from cmd :
  - to run all scenarios:  *cd to/the/project/directory*, run *mvn clean verify*;
  - to run a single scenario: *mvn integration-test -Dcucumber.filter.tags="@TEST_1"*;
3. Serenity report can be found in Gitlab pages (CI/CD->Pipelines->pages->Browse->public->index.html).

## How to write new tests?
1. Create new feature file in the *src/test/resources/features*;
2. Write new scenarios in the BDD format in the feature file using Gherkin syntax. For the further information read [**Gherkin syntax**](https://cucumber.io/docs/gherkin/);
3. Create new step definitions file in the *src/test/java/com/waarkoop/qa/steps_definitions*;
4. Write step definitions for scenarios from the feature file. Follow these links for details: [**Step Definitions**](https://cucumber.io/docs/cucumber/step-definitions/) and [**Step Organization**](https://cucumber.io/docs/gherkin/step-organization/);
5. Create new java class with the methods for calling API endpoints in the *https://cucumber.io/docs/gherkin/step-organization/*, use these methods in the step definitions;
6. Add new feature file to the @CucumberOptions/features in the *src/test/java/com/waarkoop/qa/runners/TestRunner.java*;